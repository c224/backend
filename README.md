# Backend-Setup


## สร้าง .env (สามารถ สร้างตาม .env.default)
```bash
NODE_ENV=develop
EXPRESS_PORT=7007
SIGN=test

NAME_DATABASE=TEST
HOST_DATABASE=127.0.0.1
USERNAME_DATABASE=sa
PASSWORD_DATABASE=p@ssw0rd
PORT_DATABASE=1443
```
## NODE_ENV
```bash
1. develop
2. production เมื่อทำการ deploy ขึ้นเซิฟจะต้องสรา้ง service บนเซิฟแบ่งเป็น 2 ส่วนคือ PORT dev และ PORT production
```
## Installation

```bash
 1. npm i or npm install
 2. npm run dev 
```

## Author
```bash
👤 Anon Kaedsalung
```

## License




