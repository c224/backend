const ip = require('ip');
const fs = require('fs');
const moment = require('moment');
const momentTz = require('moment-timezone');
const numeral = require('numeral')
const { env } = require('../env')
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");

exports.getIP = () => {
    return `http://${ip.address()}:5500`
}

exports.getOriginPath = (originalUrl) => {
    const replace = originalUrl.replace('/api/v1', '');
    return replace;
}

exports.getOriginFilename = (file = '') => {
    return file.replace(/.(jpg|jpeg|png|pdf)$/ig, '');
}

exports.unlinkFile = (files = []) => {
    files.forEach(e => {
        fs.unlink(e.path, () => { });
    })
}

exports.getImageOrFile = (path = '') => {
    const is_image = /.(jpg|jpeg|png)$/i.test(path)
    if (is_image) {
        return 'IMAGE'
    }

    return 'FILE'
}

exports.convertStrToFormat = (str, format) => {
    if (!str && str !== 0) {
        return '';
    }

    switch (format) {
        case 'phone_number':
            str = str.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"); break;
        case 'id_card':
            str = str.replace(/(\d{1})(\d{2})(\d{5})(\d{2})(\d{2})(\d{1})/, "$1-$2-$3-$4-$5-$6"); break;
        case 'credit':
            str = str.replace(/(\d{4})(\d{4})(\d{4})(\d{4})/, "$1-$2-$3-$4"); break;
        case 'date':
            str = moment(str).format('YYYY-MM-DD');
            break;
        case 'money_digit': str = numeral(str).format('0,0.00'); break;
        case 'time':
            const [d3, m3] = moment(str).format('HH:mm').split(':');
            str = `${d3}:${m3}`;
            break;
        case 'datetime':
            str = moment(str).format('YYYY-MM-DD HH:mm:ss')
            break;
        case 'date-th':
            str = momentTz(str).tz("Asia/Bangkok").format('DD-MM-YYYY')
            break;
    }

    return str
}

exports.convertDateMSSQL = (str) => new Date(moment(str).subtract(7, 'h'))

exports.getUrlImage = (image) => {
    return image ? `${env.ip_static}/${image}` : ''
}

exports.getPathImage = (image) => {
    image = image ? image.replace(/\\/g, '/').replace('public', 'static') : ''
    return image
}

exports.createToken = (data) => {
    const token = jwt.sign(data, env.sign, { expiresIn: "120d" });
    return token;
};

exports.hashPassword = async (password) => {
    const saltRounds = 10
    const hashPass = await bcrypt.hash(password, saltRounds);
    return hashPass
}

exports.verifyPassword = async (req_password, base_password) => {
    const validPassword = await bcrypt.compare(req_password, base_password);
    return validPassword
}