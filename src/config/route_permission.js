const route_global = [
    '/auth/login',
    '/auth/version',
    '/auth/logout'
]

const route_admin = [
    '/course/create',
    '/course/update',
    '/course/delete',
    '/course',
    '/question/default',
    '/category/default'
]

module.exports = {
    'Admin': [
        ...route_global,
        ...route_admin
    ],
    // 'USER': [
    //     ...route_global,
    //     ...route_joke
    // ],
}