const { err } = require('./debug');

exports.success = (res, message, result, code) => res.status(code || 200).json({ success: true, message, result });

exports.failed = (req, res, message, error, code) => {
    
    err(error || message, req.originalUrl);
    return res.status(code || 400).json({ success: false, message })
}
