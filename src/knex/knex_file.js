const { env } = require('../env');

module.exports = {
    development: {
        client: 'mssql', // type database
        connection: {
            host: env.db.host, // host name
            user: env.db.username,
            password: env.db.password,
            database: env.db.database, // database name
            charset: 'utf8mb4_unicode_ci',
            connectionTimeout: 30 * 100000,
            requestTimeout: 30 * 100000,
            options: {
                enableArithAbort: true,
                encrypt: false
            }
        },
        pool: {
            min: 0,
            max: 10,
        },
    },
}
