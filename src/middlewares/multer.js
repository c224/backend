const multer = require('multer');

const storage = (p) => multer.diskStorage({
    destination: function (req, file, cb) {
        let path_name = p;

        switch (file.fieldname) {
            case 'image_profile': path_name = 'images/profile'; break;
            case 'attach_files': path_name = 'images/attach'; break;
        }

        cb(null, `./public/${path_name}`);
    },
    filename(req, file, cb) {
        const split = file.originalname.split('.').length
        const fileType = file.originalname.split('.')[split - 1]
        let newImage = `${Date.now('nano')}.${fileType}`;

        switch (file.fieldname) {
            case 'image_profile':
                newImage = `profile-${Date.now('nano')}.${fileType}`;
                break;
            case 'attach_files':
                newImage = `attach-${Date.now('nano')}.${fileType}`;
                break;
        }

        cb(null, newImage);
    }
})

module.exports = (p) => multer({ storage: storage(p) });