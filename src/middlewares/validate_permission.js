const { failed } = require("../config/response");
const { getOriginPath } = require("../functions");
const routePermission = require("../config/route_permission");
const { ignoreCheckToken } = require("../utils");

exports.validate_permission = () => {

    const isAllowed = (role, originalUrl) => {
        const key = `${role}`
        const origin = getOriginPath(originalUrl);
        return routePermission[key].findIndex(e => origin.includes(e)) !== -1
    }

    return (req, res, next) => {
        const origin = getOriginPath(req.originalUrl);
        const checkIgnore = ignoreCheckToken.indexOf(origin) >= 0;
        if (checkIgnore) {
            return next();
        }

        if (req.role && isAllowed(req.role, req.originalUrl)) {
            next();
        } else {
            failed(req, res, 'Permission denied', '', 403)
        }
    }
}
