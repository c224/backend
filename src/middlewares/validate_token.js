const jsonwebtoken = require('jsonwebtoken');
const { failed } = require('../config/response');
const { err, debug } = require('../config/debug');
const { getOriginPath } = require('../functions');
const { ignoreCheckToken } = require('../utils');


exports.validate_token = () => {
    return (req, res, next) => {
        if (!req.originalUrl) {
            return failed(req, res, NULL, 404);
        }

        const origin = getOriginPath(req.originalUrl);

        const checkIgnore = ignoreCheckToken.indexOf(origin) >= 0;
        if (checkIgnore) {
            return next();
        }

        if (req.headers && req.headers.authorization) {
            jsonwebtoken.verify(req.headers.authorization, process.env.SIGN, (error, decode) => {
                if (error) {
                    err('token not found', req.originalUrl)
                    failed(req, res, 'token not found')
                } else {

                    req.token = req.headers.authorization
                    req.account_id = decode.account_id
                    req.full_name = decode.full_name
                    req.role_id = decode.role_id
                    req.role = decode.role

                    next();
                }
            })
        } else {
            // req.headers.authorization = null;
            err('token not found', req.originalUrl)
            failed(req, res, 'token invalidate');
        }
    }
}
