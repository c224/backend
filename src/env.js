require("../config_env");

const { getOsEnv } = require("./utils/utils");

exports.env = {
    sign: getOsEnv('SIGN'),
    node: process.env.NODE_ENV,
    port: process.env.EXPRESS_PORT,
    db: {
        host: getOsEnv('HOST_DATABASE'),
        port: getOsEnv('PORT_DATABASE'),
        username: getOsEnv('USERNAME_DATABASE'),
        password: getOsEnv('PASSWORD_DATABASE'),
        database: getOsEnv('NAME_DATABASE')
    },
    ip_static:getOsEnv('IP_STATIC')
}
