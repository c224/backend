const courseModel = require('./course_model');
const { success, failed } = require('../../config/response');
const { convertStrToFormat } = require('../../functions')


class CourseController {

    async listCourse(req, res) {
        try {
            const { filter_cate } = req.query
            const query = await courseModel.listCourse(filter_cate)


            const arr_course_id = [...new Set(query.map((e) => e.course_id))]
            const arr_chapter_id = [...new Set(query.map((e) => e.chapter_id))]
            const arr_lab_id = [...new Set(query.map((e) => e.lab_id))]
            const arr_question_id = [...new Set(query.map((e) => e.question_id))]
            const arr_choice_id = [...new Set(query.map((e) => e.choice_id))]
            const arr_category_id = [...new Set(query.map((e) => e.type_cate_course_id))]

            const result = arr_course_id.map((el1) => {
                const find = query.find((el2) => el2.course_id === el1);
                const { course_id, course_name, course_desc, course_status, create_date, type_cate_id } = find
                const isID = type_cate_id
                const categories = arr_category_id.map((el) => {
                    const find = query.find((e) => e.type_cate_course_id === el)
                    const { type_cate_id, type_cate_name } = find

                    if (course_id === find.course_id && isID)
                        return { type_cate_id, type_cate_name }
                })
                const chapters = arr_chapter_id.map((el3) => {
                    const find = query.find((el4) => el4.chapter_id === el3);
                    const { chapter_id, chapter_name, chapter_desc, chapter_status } = find

                    const labs = arr_lab_id.map((el5) => {
                        const find = query.find((el6) => el6.lab_id === el5);
                        const { lab_id, lab_name, lab_status } = find

                        const questions = arr_question_id.map((el7) => {
                            const find = query.find((el8) => el8.question_id === el7)
                            const { question_id, question, question_status, question_type } = find

                            const choices = arr_choice_id.map((el9) => {
                                const find = query.find((el10) => el10.choice_id === el9)
                                const { choice_id, choice, choice_status } = find

                                if (question_id === find.question_id && choice_id) {
                                    return {
                                        choice_id,
                                        choice,
                                        choice_status
                                    }
                                }
                            })

                            if (lab_id === find.lab_id && lab_id) {
                                return {
                                    question_id,
                                    question,
                                    question_status,
                                    question_type,
                                    question_choices: choices.filter((e) => e)
                                }
                            }

                        })

                        if (chapter_id === find.chapter_id && chapter_id) {
                            return {
                                lab_id,
                                lab_name,
                                lab_status,
                                questions: questions.filter((e) => e)
                            }
                        }

                    })

                    if (course_id === find.course_id && chapter_id) {
                        return {
                            chapter_id,
                            chapter_name,
                            chapter_desc,
                            chapter_status,
                            labs: labs.filter((e) => e)
                        }
                    }
                })


                return {
                    course_id,
                    create_date: convertStrToFormat(create_date, 'date-th'),
                    categories: categories.filter((e) => e),
                    course_name,
                    course_desc,
                    course_status,
                    chapters: chapters.filter((e) => e)
                }
            })

            success(res, 'ดึงข้อมูลสำเร็จ', result);
        } catch (error) {
            console.log(error)
            failed(req, res, 'ดึงข้อมูลไม่สำเร็จ')
        }
    }

    async createCourse(req, res) {
        try {
            const { course_name, course_desc, chapter, categories } = req.body;

            const obj = { course_name, course_desc, chapter, categories }
            const query = await courseModel.createCourse(obj)

            if (!query || query.length <= 0) {
                failed(req, res, 'สร้างข้อมูลไม่สำเร็จ')
            }

            if (!query[0].success) {
                failed(req, res, 'สร้างข้อมูลไม่สำเร็จ')
            }

            success(res, 'สร้างข้อมูลสำเร็จ');
        } catch (error) {
            console.log(error)
            failed(req, res, 'สร้างข้อมูลไม่สำเร็จ')
        }
    }

    async updateCourse(req, res) {
        try {
            const { course_id, course_name, course_desc, chapter, categories } = req.body;

            const obj = { course_id, course_name, course_desc, chapter, categories }
            const query = await courseModel.updateCourse(obj)

            if (!query || query.length <= 0) {
                failed(req, res, 'แก้ไขข้อมูลไม่สำเร็จ')
            }

            if (!query[0].success) {
                failed(req, res, 'แก้ไขข้อมูลไม่สำเร็จ')
            }


            success(res, 'แก้ไขข้อมูลสำเร็จ');
        } catch (error) {
            console.log(error)
            failed(req, res, 'แก้ไขข้อมูลไม่สำเร็จ')
        }
    }

    async deleteCourse(req, res) {
        try {
            const { course_id } = req.query;
            const query = await courseModel.deleteCourse(course_id)

            if (query.length <= 0) {
                failed(req, res, 'ลบข้อมูลไม่สำเร็จ')
            }

            success(res, 'ลบข้อมูลสำเร็จ');
        } catch (error) {
            console.log(error)
            failed(req, res, 'ลบข้อมูลไม่สำเร็จ')
        }
    }

}

module.exports = new CourseController();