const { knex } = require('../../knex')
const { DataTable, CategoriesTable } = require('./course_table')

class CourseModel {

    listCourse(filter_cate = '') {
        return knex.raw(`EXEC Sp_Course_List ?`, [filter_cate])
    }

    async createCourse(obj) {
        const { course_name, course_desc, chapter, categories } = obj

        const { chapterTable, labTable, questionTable, choiceTable } = DataTable(chapter)

        const { categoryTable } = CategoriesTable(categories)

        const connection = await knex.client.acquireConnection();
        const result = await connection.request()
            .input('course_name', course_name)
            .input('course_desc', course_desc)
            .input('chapter_table', chapterTable)
            .input('lab_table', labTable)
            .input('question_table', questionTable)
            .input('choice_table', choiceTable)
            .input('category_table', categoryTable)
            .execute('Sp_Course_Create')

        knex.client.releaseConnection(connection);
        return result.recordset;
    }

    async updateCourse(obj) {
        const { course_id, course_name, course_desc, chapter, categories } = obj

        const { chapterTable, labTable, questionTable, choiceTable } = DataTable(chapter)

        const { categoryTable } = CategoriesTable(categories)
        console.log(`chapterTable`, chapterTable)
        console.log(`labTable`, labTable)
        console.log(`questionTable`, questionTable)
        console.log(`choiceTable`, choiceTable)
        const connection = await knex.client.acquireConnection();
        const result = await connection.request()
            .input('course_id', course_id)
            .input('course_name', course_name)
            .input('course_desc', course_desc)
            .input('chapter_table', chapterTable)
            .input('lab_table', labTable)
            .input('question_table', questionTable)
            .input('choice_table', choiceTable)
            .input('category_table', categoryTable)
            .execute('Sp_Course_Update')

        knex.client.releaseConnection(connection);
        return result.recordset;
    }

    deleteCourse(course_id) {
        return knex.raw(`EXEC Sp_Course_Delete ?`, [course_id])
    }

}

module.exports = new CourseModel();