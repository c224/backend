const mssql = require('mssql');


const initialChapterTable = () => {
    const chapterTable = new mssql.Table('ChapterTable');
    chapterTable.columns.add('index_chapter', mssql.Int);
    chapterTable.columns.add('chapter_id', mssql.VarChar(20));
    chapterTable.columns.add('chapter_name', mssql.NVarChar(mssql.MAX));
    chapterTable.columns.add('chapter_desc', mssql.NVarChar(mssql.MAX));
    chapterTable.columns.add('type', mssql.NVarChar(20));

    return chapterTable
}

const initialLabTable = () => {
    const LabTable = new mssql.Table('LabTable');
    LabTable.columns.add('index_lab', mssql.Int);
    LabTable.columns.add('index_chapter', mssql.Int);
    LabTable.columns.add('lab_id', mssql.VarChar(20));
    LabTable.columns.add('lab_name', mssql.NVarChar(mssql.MAX));
    LabTable.columns.add('type', mssql.NVarChar(20));

    return LabTable
}

const initialQuestionTable = () => {
    const QuestionTable = new mssql.Table('QuestionTable');
    QuestionTable.columns.add('index_question', mssql.Int);
    QuestionTable.columns.add('index_lab', mssql.Int);
    QuestionTable.columns.add('index_chapter', mssql.Int);
    QuestionTable.columns.add('question_id', mssql.VarChar(20));
    QuestionTable.columns.add('question', mssql.NVarChar(mssql.MAX));
    QuestionTable.columns.add('question_type', mssql.NVarChar(50));
    QuestionTable.columns.add('type', mssql.NVarChar(20));

    return QuestionTable
}

const initialChoiceTable = () => {
    const ChoiceTable = new mssql.Table('ChoiceTable');
    ChoiceTable.columns.add('index_choice', mssql.Int);
    ChoiceTable.columns.add('index_question', mssql.Int);
    ChoiceTable.columns.add('index_lab', mssql.Int);
    ChoiceTable.columns.add('index_chapter', mssql.Int);
    ChoiceTable.columns.add('choice_id', mssql.VarChar(20));
    ChoiceTable.columns.add('choice', mssql.NVarChar(100));
    ChoiceTable.columns.add('type', mssql.NVarChar(20));

    return ChoiceTable
}

const initialCategoriesTable = () => {
    const categoryTable = new mssql.Table('CategoriesTable');
    categoryTable.columns.add('index_category', mssql.Int);
    categoryTable.columns.add('category_id', mssql.NVarChar(20));

    return categoryTable
}

exports.CategoriesTable = (categories = []) => {
    const categoryTable = initialCategoriesTable();
    console.log(`categories`, categories)
    for (const index_category in categories) {
        categoryTable.rows.add(index_category, categories[index_category])
    }

    return { categoryTable }
}

exports.DataTable = (chapters = []) => {
    const chapterTable = initialChapterTable();
    const labTable = initialLabTable();
    const questionTable = initialQuestionTable();
    const choiceTable = initialChoiceTable();

    for (const indexChapter in chapters) {
        const { chapter_id, chapter_name, chapter_desc, labs } = chapters[indexChapter]
        const typeChapter = !chapters[indexChapter].type ? '' : chapters[indexChapter].type
        chapterTable.rows.add(indexChapter, !chapter_id ? '' : chapter_id, chapter_name, chapter_desc, typeChapter)

        for (const indexLab in labs) {
            const { questions, lab_id, lab_name } = labs[indexLab]
            const typeLab = !labs[indexLab].type ? '' : labs[indexLab].type
            labTable.rows.add(indexLab, indexChapter, !lab_id ? '' : lab_id, lab_name, typeLab)

            for (const indexQuestion in questions) {
                const { question_id, question, question_type, question_choices } = questions[indexQuestion]
                const typeQ = !questions[indexQuestion].type ? '' : questions[indexQuestion].type
                questionTable.rows.add(indexQuestion, indexLab, indexChapter, !question_id ? '' : question_id, question, question_type, typeQ)

                for (const indexChoice in question_choices) {
                    const { choice_id, choice } = question_choices[indexChoice]
                    const typeCho = !question_choices[indexChoice].type ? '' : question_choices[indexChoice].type
                    choiceTable.rows.add(indexChoice, indexQuestion, indexLab, indexChapter, !choice_id ? '' : choice_id, choice, typeCho)
                }
            }
        }
    }

    return { chapterTable, labTable, questionTable, choiceTable }
}

