const { Router } = require('express');
const courseController = require('./course_controller');
const { validate_schema } = require('../../middlewares/validate_schema')
const { schemaCreate, schemaDelete, schemaUpdate } = require('./course_schema');
const courseRoute = Router();


courseRoute.get('/',
    courseController.listCourse
)

courseRoute.post('/create',
    validate_schema(schemaCreate),
    courseController.createCourse
)

courseRoute.post('/update',
    validate_schema(schemaUpdate),
    courseController.updateCourse
)

courseRoute.delete('/delete',
    validate_schema(schemaDelete, 'query'),
    courseController.deleteCourse
)

module.exports = courseRoute;