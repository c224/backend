const Joi = require('joi');


exports.schemaCreate = Joi.object({
    course_name: Joi.string().required(),
    course_desc: Joi.string().required(),
    chapter: Joi.array().allow(''),
    categories: Joi.array().required()
})

exports.schemaUpdate = Joi.object({
    course_id: Joi.string().required(),
    course_name: Joi.string().required(),
    course_desc: Joi.string().required(),
    chapter: Joi.array().allow(''),
    chapters: Joi.array().allow(''),
    course_status: Joi.string().allow(''),
    create_date: Joi.string().allow(''),
    categories: Joi.array().required()
})

exports.schemaDelete = Joi.object({
    course_id: Joi.string().required()
})

