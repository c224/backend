const { success, failed } = require('../../config/response')
const jokeModel = require('./joke_model')

class JokeController {

    async getAllJoke(req, res) {
        try {

            const query = await jokeModel.getAllJoke()

            success(res, 'ดึงข้อมูลสำเร็จ', query)
        } catch (error) {
            console.log(error);
            failed(req, res, 'ดึงข้อมูลไม่สำเร็จ')
        }
    }

    async createJoke(req, res) {
        try {
            const obj = {
                ...req.body,
                action_type: "CREATE"
            }

            await jokeModel.upsertJoke(obj)

            success(res, 'สร้างข้อมูลสำเร็จ')
        } catch (error) {
            console.log(error);
            failed(req, res, 'สร้างข้อมูลไม่สำเร็จ')
        }
    }

    async updateJoke(req, res) {
        try {
            const obj = {
                ...req.body,
                action_type: "UPDATE"
            }

            await jokeModel.upsertJoke(obj)

            success(res, 'อัพเดทข้อมูลสำเร็จ')
        } catch (error) {
            console.log(error);
            failed(req, res, 'อัพเดทข้อมูลไม่สำเร็จ')
        }
    }

    async deleteJoke(req, res) {
        try {
            const { id: joke_id } = req.params

            await jokeModel.deleteJoke(joke_id || 0)

            success(res, 'ลบข้อมูลสำเร็จ')
        } catch (error) {
            console.log(error);
            failed(req, res, 'ลบข้อมูลไม่สำเร็จ')
        }
    }

    async detailJoke(req, res) {
        try {
            const { id: joke_id } = req.params

            const query = await jokeModel.getJokeById(joke_id || 0)

            if (query.length <= 0)
                failed(req, res, 'ไม่พบข้อมูล')

            success(res, 'ดึงข้อมูลสำเร็จ', query[0])
        } catch (error) {
            console.log(error);
            failed(req, res, 'ดึงข้อมูลไม่สำเร็จ')
        }
    }

    async likeJoke(req, res) {
        try {
            const { id: joke_id } = req.params

            await jokeModel.likeJoke(joke_id || 0)

            success(res, 'like ข้อมูลสำเร็จ')
        } catch (error) {
            console.log(error);
            failed(req, res, 'like ข้อมูลไม่สำเร็จ')
        }
    }

    async disLikeJoke(req, res) {
        try {
            const { id: joke_id } = req.params

            await jokeModel.disLikeJoke(joke_id || 0)

            success(res, 'DisLike ข้อมูลสำเร็จ')
        } catch (error) {
            console.log(error);
            failed(req, res, 'DisLike ข้อมูลไม่สำเร็จ')
        }
    }

}

module.exports = new JokeController();