const Joi = require('joi');


exports.schemaCreateJoke = Joi.object({
    joke_name: Joi.string().required(),
    joke_desc: Joi.string().required(),
})

exports.schemaUpdateJoke = Joi.object({
    joke_name: Joi.string().required(),
    joke_desc: Joi.string().required(),
    joke_id: Joi.number().required()
})

exports.schemaJokeById = Joi.object({
    joke_id: Joi.number().required()
})

