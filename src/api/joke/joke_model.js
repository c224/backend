const { knex } = require('../../knex')


class JokeModel {

    getAllJoke() {
        return knex.raw(`EXEC Sp_Joke_GetALL`);
    }

    upsertJoke(obj) {
        const { joke_name, joke_desc, joke_id = 0, action_type } = obj
        return knex.raw(`EXEC Sp_Joke_Upsert ?,?,?,?`, [joke_name, joke_desc, joke_id, action_type]);
    }

    deleteJoke(joke_id) {
        return knex.raw(`EXEC Sp_Joke_Delete ?`, [joke_id]);
    }

    getJokeById(joke_id) {
        return knex.raw(`EXEC Sp_Joke_GetDetail ?`, [joke_id]);
    }

    likeJoke(joke_id) {
        return knex.raw(`EXEC Sp_Joke_Like ?`, [joke_id]);
    }

    disLikeJoke(joke_id) {
        return knex.raw(`EXEC Sp_Joke_DisLike ?`, [joke_id]);
    }
}

module.exports = new JokeModel();