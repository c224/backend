const { Router } = require('express')
const { validate_schema } = require('../../middlewares/validate_schema')
const jokeController = require('./joke_controller')
const { schemaCreateJoke, schemaUpdateJoke } = require('./joke_schema')

const jokeRoute = Router()

jokeRoute.get('/',
    jokeController.getAllJoke
);

jokeRoute.post('/',
    validate_schema(schemaCreateJoke),
    jokeController.createJoke
);

jokeRoute.put('/',
    validate_schema(schemaUpdateJoke),
    jokeController.updateJoke
);

jokeRoute.get('/:id',
    jokeController.detailJoke
);

jokeRoute.delete('/:id',
    jokeController.deleteJoke
);

jokeRoute.post('/:id/like',
    jokeController.likeJoke
);

jokeRoute.post('/:id/dislike',
    jokeController.disLikeJoke
);

module.exports = jokeRoute;

