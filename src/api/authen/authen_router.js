const { Router } = require('express');
const authenController = require('./authen_controller');
const { validate_schema } = require('../../middlewares/validate_schema')
const { schemaLogin, schemaRegister } = require('./authen_schema');
const multer = require('../../middlewares/multer')

const authenRoute = Router();

authenRoute.post('/login',
    validate_schema(schemaLogin),
    authenController.login
)

authenRoute.post('/register',
    multer().fields([
        { name: 'image_profile', maxCount: 1 }
    ]),
    // validate_schema(schemaRegister),
    authenController.createAccount
)

module.exports = authenRoute