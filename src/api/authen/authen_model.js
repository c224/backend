const { knex } = require('../../knex')
const mssql = require('mssql');

class AuthenModel {

    login(email) {
        return knex.raw(`EXEC Sp_Authen_Login ?`, [email]);
    }

    createUser(obj) {
        const { email, full_name, hash_password: password, gender_id, role_id, image_profile } = obj
        return knex.raw(`EXEC Sp_Authen_Create ?,?,?,?,?,?`, [email, full_name, password, gender_id, role_id, image_profile]);
    }
}

module.exports = new AuthenModel();
