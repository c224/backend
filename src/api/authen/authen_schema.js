const Joi = require('joi');


exports.schemaLogin = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
})

exports.schemaRegister = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
    full_name: Joi.string().required(),
    gender_id: Joi.string().required(),
    role_id: Joi.string().required().allow(null, '').valid('R-0001', 'R-0002')
})

