const authenModel = require('./authen_model');
const { success, failed } = require('../../config/response');
const { hashPassword, verifyPassword, createToken, getPathImage, unlinkFile, getUrlImage } = require('../../functions')
const moment = require('moment')

class AuthenController {

    async login(req, res) {
        try {
            const { email, password } = req.body;

            const query = await authenModel.login(email)

            if (query.length <= 0) {
                failed(req, res, 'ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง!', '', 401)
            }

            const { account_id
                , full_name
                , acc_status
                , create_date
                , email: base_email
                , image_profile
                , password: base_password
                , role
                , role_id
                , gender_name } = query[0]

            const valid = await verifyPassword(password, base_password)

            const obj = {
                account_id,
                full_name,
                role,
                role_id
            }
            
            if (!valid) {
                failed(req, res, 'ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง!')
            }

            req.token = createToken(obj)
            const result = {
                account_id,
                full_name,
                acc_status,
                create_date,
                email: base_email,
                image_profile: getUrlImage(image_profile),
                role,
                gender_name,
                token: req.token,
                date_login: moment().unix(),
            }
            success(res, 'สร้างข้อมูลสำเร็จ', result);
        } catch (error) {
            console.log(error);
            failed(req, res, 'สร้างข้อมูลไม่สำเร็จ')
        }

    }

    async createAccount(req, res) {
        const files = JSON.parse(JSON.stringify(req.files))
        const { image_profile } = files
        try {

            const { email, full_name, password, gender_id, role_id = 'R-0002' } = req.body;
            const path = image_profile.map((e) => getPathImage(e.path));

            const hashP = await hashPassword(password)
            const obj = {
                hash_password: hashP,
                email,
                full_name,
                gender_id,
                role_id,
                image_profile: path
            }

            const query = await authenModel.createUser(obj)

            if (query.length <= 0) {
                unlinkFile(image_profile);
                failed(req, res, 'สร้างบัญชีผู้ใช้ไม่สำเร็จ')
            }


            success(res, 'สร้างบัญชีผู้ใช้สำเร็จ');
        } catch (error) {
            console.log(error);
            unlinkFile(image_profile);
            failed(req, res, 'สร้างบัญชีผู้ใช้ไม่สำเร็จ')
        }
    }
}

module.exports = new AuthenController();