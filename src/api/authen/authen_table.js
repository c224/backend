const mssql = require('mssql');


const initialMediaDisplayImageUpdateTable = () => {
    const mediaDisplayImageUpdateTable = new mssql.Table('MediaDisplayImageUpdateTable');
    mediaDisplayImageUpdateTable.columns.add('indexImg', mssql.Int);
    mediaDisplayImageUpdateTable.columns.add('mdi_id', mssql.NVarChar(20));
    mediaDisplayImageUpdateTable.columns.add('imagePath', mssql.NVarChar(100));

    return mediaDisplayImageUpdateTable
}

export const MediaDisplayImageUpdateTable = (images = []) => {
    const mediaDisplayImageUpdateTable = initialMediaDisplayImageUpdateTable();
    for (const val in images) {
        const { mdi_id, image } = images[val]
        mediaDisplayImageUpdateTable.rows.add(val, mdi_id, image)
    }

    return { mediaDisplayImageUpdateTable }
}

