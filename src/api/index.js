const { validate_token } = require('../middlewares/validate_token')
const { validate_permission } = require('../middlewares/validate_permission')
const courseRouter = require('./course/course_router');
const authenRouter = require('./authen/authen_router');
const questionRouter = require('./question/question_router');
const categoryRouter = require('./category/category_router');

exports.createApi = async (app, version = '/api/v1') => {
    app.use(validate_token())
    app.use(validate_permission())

    app.use(version + '/authen', authenRouter)
    app.use(version + '/course', courseRouter)
    app.use(version + '/question', questionRouter)
    app.use(version + '/category', categoryRouter)
}
