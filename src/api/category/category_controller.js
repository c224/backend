const { success, failed } = require('../../config/response');
const categoryModel = require('./category_model')

class CategoryController {

    async listCategoryType(req, res) {
        try {

            const query = await categoryModel.listCategoryType()

            if (query.length <= 0) {
                failed(req, res, 'ดึงข้อมูลไม่สำเร็จ')
            }

            success(res, 'ดึงข้อมูลสำเร็จ', query);
        } catch (error) {
            console.log(error)
            failed(req, res, 'ดึงข้อมูลไม่สำเร็จ')
        }
    }
}

module.exports = new CategoryController()