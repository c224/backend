const { Router } = require('express');
const categoryController = require('./category_controller');
const { validate_schema } = require('../../middlewares/validate_schema')
const categoryRoute = Router();

categoryRoute.get('/default',
    categoryController.listCategoryType
)


module.exports = categoryRoute;