const { knex } = require('../../knex')

class CategoryModel {

    listCategoryType() {
        return knex.raw(`EXEC Sp_Category_ListType`)
    }
    
}

module.exports = new CategoryModel()