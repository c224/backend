const { success, failed } = require('../../config/response');
const questionModel = require('./question_model')

class QuestionController {

    async listQuestionType(req, res) {
        try {

            const query = await questionModel.listQuestionType()

            if (query.length <= 0) {
                failed(req, res, 'ดึงข้อมูลไม่สำเร็จ')
            }

            success(res, 'ดึงข้อมูลสำเร็จ', query);
        } catch (error) {
            console.log(error)
            failed(req, res, 'ดึงข้อมูลไม่สำเร็จ')
        }
    }
}

module.exports = new QuestionController()