const { Router } = require('express');
const questionController = require('./question_controller');
const { validate_schema } = require('../../middlewares/validate_schema')
const {  } = require('./question_schema');
const questionRoute = Router();

questionRoute.get('/default',
    questionController.listQuestionType
)


module.exports = questionRoute;