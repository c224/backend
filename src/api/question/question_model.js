const { knex } = require('../../knex')

class QuestionModel {

    listQuestionType() {
        return knex.raw(`EXEC Sp_Question_ListType`)
    }
    
}

module.exports = new QuestionModel()