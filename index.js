const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const dayjs = require('dayjs');
const ratelimit = require('express-rate-limit');
const helmet = require('helmet');
const path = require('path')
const { createApi } = require('./src/api')
const { debug } = require('./src/config/debug');
const { env } = require("./src/env");
const moment = require('moment');

require('dayjs/locale/th')
require('dotenv').config();
require('./src/knex');

dayjs.locale('th')
const app = express();
const version = '/api/v1';

/*###################### SETTING ######################*/
app.use(helmet());
app.use('/static', express.static(path.join(__dirname, './public')));

const whitelist = [
    `http://139.59.229.66:8080`,
    `http://139.59.229.66`,
    `http://localhost:8080`,
    undefined
]

const corsOption = {
    origin: (origin, cb) => {
        debug('origin %o', origin);
        if (whitelist.indexOf(origin) !== -1) {
            cb(null, true)
        } else {
            cb(new Error('Not allows by Cors'))
        }
    },
}

app.use(cors(corsOption));
app.use(bodyparser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyparser.json({ limit: '50mb' }));


const limiter = ratelimit({
    windowsMs: 60 * 1000,
    max: 200
})

app.use(limiter)

/*###################### DEBUG ######################*/
app.use((req, res, next) => {
    debug(`%s origin (${process.pid}) %o`, moment().format('YYYY-MM-DD HH:mm:ss'), req.originalUrl);
    next();
});

/*###################### CREATE API ######################*/
createApi(app, version);

switch (`${env.node}`) {
    case 'develop':
        app.listen(env.port, () => {
            debug(`[${env.node}] ::: Server is running port ${env.port}`)
        });
        break;
    case 'production':
        const cluster = require('cluster');
        const { length } = require('os').cpus()
        const numCPUs = length > 4 ? 4 : length;
        if (cluster.isMaster) {
            for (let i = 0; i < numCPUs; i++) {
                cluster.fork();
            }
            debug(`Server is running port (${numCPUs} CPUs) : ${env.port}`)
            cluster.on('exit', (worker) => console.log(`worker ${worker.process.pid} died`));
        } else {
            app.listen(env.port, () => debug(`Server is running port (${process.pid}): ${env.port}`))
        }
        break;
}


